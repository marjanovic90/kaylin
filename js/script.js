var offsetToTriggerAnimation = 3000; //svgLocation.y + svgLocation.height;
let firstDate;
let lastDate;
let sheetData = [];
const sheetURL =
  "https://docs.google.com/spreadsheets/d/123TxW7IJYgl6xqSnDLfBcO8opCS9JP9VjIWi6_PfpwU/edit?usp=sharing";

const chartDiv = document.getElementById("chart");
const currencyPicker = document.getElementById("currencyPicker");
const btcSpan = document.getElementById("btcCalc");
const moneySpan = document.getElementById("moneyCalc");
const yieldCurrencySpan = document.getElementById("yield-currency");
let currency = "€";
let dollarMultiplier = 1;
let funds = 10000;
const fundsInput = document.getElementById("funds");

const currencyDict = {
  $: "USD",
  "€": "EUR"
};

fundsInput.addEventListener("input", e => {
  funds = e.target.value ? Number(e.target.value) : 10000;
  updateFunds();
  drawVisualization();
});

currencyPicker.addEventListener("change", e => {
  currency = e.target.value;
  yieldCurrencySpan.textContent = currencyDict[currency];
  updateFunds();
  drawVisualization();
});

// formats date for datepickers
function formatPickerDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}

function updateFunds() {
  const start = sheetData.find(
    item => item.date.getTime() === firstDate.getTime()
  );
  const end = sheetData.find(
    item => item.date.getTime() === lastDate.getTime()
  );

  if (start && end) {
    var z = (end.growth * funds) / start.growth;
    var c = (end.btc / start.btc) / (end.growth / start.growth);

    if (currency === "$") {
      z *= dollarMultiplier;
    }

    moneySpan.textContent = currency + z.toFixed(2);
    btcSpan.textContent = c.toFixed(2);
  }
}

// formats given data date into valid date
function getValidDate(date) {
  const [day, month, year] = date.split(" ");
  return new Date(...[year, month - 1, day].map(i => Number(i)));
}
// Function to handle the scroll event.
// Add an event handler to the document for the "onscroll" event
function scrollAnimTriggerCheck(evt, elem) {
  var viewBottom = window.scrollY + window.innerHeight;
  if (viewBottom > offsetToTriggerAnimation) {
    // Start the CSS animation by setting the animation-play-state to "running"
    var els = document.querySelectorAll("div");
    document
      .querySelectorAll(".donut-segment")
      .forEach(el => (el.style.animationPlayState = "running"));
    document.removeEventListener("scroll", scrollAnimTriggerCheck);
  }
}

// Add an event handler to the document for the "onscroll" event
document.addEventListener("scroll", scrollAnimTriggerCheck);

$(window).on("scroll", function() {
  if ($(window).scrollTop() > 88) {
    $("#header").addClass("down");
    $(".nav-collapse a").addClass("downi");
  } else {
    //remove the background property
    $("#header").removeClass("down");
    $(".nav-collapse a").removeClass("downi");
  }
});

google.load("visualization", "1.1", {
  packages: ["corechart", "controls"]
});

function initSheet() {
  Tabletop.init({
    key: sheetURL,
    parseNumbers: true,
    callback: portfolio => {
      sheetData = portfolio.filter(item => item.date).map(item => ({
        date: getValidDate(item.date),
        growth: item["change in percentages to the initial value (W)"],
        startValue: item["initial value"],
        portfolio: item["in euro"],
        market: item["market on average by day"],
        btc: item["Price of BTC daily"]
      }));
      // firstDate = sheetData[sheetData.length - 61].date;
      firstDate = sheetData[0].date;
      lastDate = sheetData[sheetData.length - 1].date;

      $("#start-datepicker").datepicker(
        "setDate",
        new Date(formatPickerDate(firstDate))
      );

      $("#end-datepicker").datepicker(
        "setDate",
        new Date(formatPickerDate(lastDate))
      );
      drawVisualization();
    },
    simpleSheet: true
  });
}

function drawVisualization() {
  var dashboard = new google.visualization.Dashboard(
    document.getElementById("dashboard")
  );
  var control = new google.visualization.ControlWrapper({
    controlType: "ChartRangeFilter",
    containerId: "control",
    options: {
      filterColumnIndex: 0,
      ui: {
        chartType: "LineChart",
        chartOptions: {
          chartArea: {
            width: "inherit"
          },
          hAxis: {
            baselineColor: "none",
            format: "dd.MM.yyyy"
          }
        },
        chartView: {
          columns: [0, 2, 1]
        },
        minRangeSize: 3196800000
      }
    },
    state: {
      range: {
        start: firstDate,
        end: lastDate
      }
    }
  });

  const curencyFormatter = new google.visualization.NumberFormat({
    prefix: currency
  });

  var chart = new google.visualization.ChartWrapper({
    chartType: "AreaChart",
    containerId: "chart",
    options: {
      backgroundColor: "#E1E0EF",
      isStacked: false,
      chartArea: {
        height: "80%",
        width: "inherit"
      },
      hAxis: {
        slantedText: false,
        textStyle: { color: "#30339C", fontWeight: "800" },
        baselineColor: "#30339C"
      },
      vAxis: {
        format: "short",
        textStyle: { color: "#30339C", fontWeight: "800" },
        baselineColor: "#30339C"
      },
      colors: ["red", "#30339C"],
      legend: { position: "top", maxLines: 1, alignment: "end" }
    },
    view: {
      columns: [
        {
          calc: function(dataTable, rowIndex) {
            return dataTable.getFormattedValue(rowIndex, 0);
          },
          type: "string"
        },
        1,
        2
      ]
    }
  });

  // number of days between start and end date
  var daysRange = (lastDate - firstDate) / 1000 / 60 / 60 / 24;

  // how many days to skip
  var skipEvery = 1;

  switch (true) {
    case daysRange > 400:
      skipEvery = 7;
      break;
    case daysRange > 300:
      skipEvery = 5;
      break;
    case daysRange > 200:
      skipEvery = 3;
      break;
    default:
      skipEvery = 1;
  }

  const start = sheetData.find(
    item => item.date.getTime() === firstDate.getTime()
  );

  var data = new google.visualization.DataTable({
    cols: [
      { label: "Date", type: "date" },
      { label: "Market", type: "number" },
      { label: "Portfolio", type: "number" }
    ],
    rows: sheetData
      .filter((item, index) => index % skipEvery === 0)
      .map(item => ({
        c: [
          { v: item.date },
          {
            v:
              currency === "$"
                ? (dollarMultiplier *
                    ((item.market / item.startValue) * funds)) /
                  (start.market / start.startValue)
                : ((item.market / item.startValue) * funds) /
                  (start.market / start.startValue)
          },
          {
            v:
              currency === "$"
                ? (dollarMultiplier *
                    ((item.portfolio / item.startValue) * funds)) /
                  (start.portfolio / start.startValue)
                : ((item.portfolio / item.startValue) * funds) /
                  (start.portfolio / start.startValue)
          }
        ]
      }))
  });

/* Change the format of the date column, used in chart, but not chart range filter */
  var formatter = new google.visualization.DateFormat({
    pattern: 'dd.MM.yyyy'
  });
  formatter.format(data, 0);
  curencyFormatter.format(data, 1);
  curencyFormatter.format(data, 2);

  dashboard.bind(control, chart);
  dashboard.draw(data);


function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    }, wait);
    if (immediate && !timeout) func.apply(context, args);
  };
}



const optimizedRangeEvent = debounce(() => {
  var state = control.getState();
    let sd = control.I.range.start.toDateString();
    let ed = control.I.range.end.toDateString();

    if (sd && ed) {
      $('#start-datepicker').datepicker(
        'setDate',
        new Date(formatPickerDate(sd))
      );
      $('#end-datepicker').datepicker(
        'setDate',
        new Date(formatPickerDate(ed))
      );
      firstDate = new Date(sd);
      lastDate = new Date(ed);
      updateFunds();
    }
}, 1000)


  //onclick displaying
  google.visualization.events.addListener(control, 'statechange', function() {
    optimizedRangeEvent()
  });

  google.visualization.events.addListener(chart, 'ready', function() {
    var axisLabels = chartDiv.getElementsByTagName('text');
    for (var i = 0; i < axisLabels.length; i++) {
      if (axisLabels[i].getAttribute('text-anchor') === 'end') {
        if(!axisLabels[i].innerHTML.includes(currency)) {
          axisLabels[i].innerHTML = currency + axisLabels[i].innerHTML;
        }
        
      }
    }
  });
}
google.setOnLoadCallback(drawVisualization);
$(window).resize(function() {
  drawVisualization();
});

$(document).ready(function() {
  $.getJSON("https://api.exchangeratesapi.io/latest", data => {
    dollarMultiplier = data.rates.USD;
  });
  updateFunds();
  initSheet();

  $("#start-datepicker").datepicker({
    autoHide: true,
    zIndex: 2048,
    pick: ({ date }) => {
      firstDate = new Date(date);
      updateFunds();
      drawVisualization();
    }
  });

  $("#end-datepicker").datepicker({
    autoHide: true,
    zIndex: 2048,
    pick: ({ date }) => {
      lastDate = new Date(date);
      updateFunds();
      drawVisualization();
    }
  });

  // if ($.browser && $.browser.mobile === true) {
  //   document.getElementById('gdata').style.display = 'none';
  //   document.getElementById('control').style.display = 'none';
  // }
});
